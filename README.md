# Backend-Control3-Tel335



## Getting started

Luego de haber clonado este repositorio, ejecute el siguiente comando en la terminal, en la ubicación del proyecto, para instalar las dependencias. 
```
npm install
```
Luego, para iniciar la API, ejecute:
```
npm start
```

La API va a estar corriendo en el puerto 3001 en nuestro localhost. Para comprobar su funcionamiento, puede dirigirse a su navegador y colocar en el buscador localhost:3001/phrase




