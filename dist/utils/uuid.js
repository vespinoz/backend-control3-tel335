"use strict";

var _uuid = require("uuid");

exports.getUUIDV4 = () => {
  return (0, _uuid.v4)();
};