"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SECRET = exports.KEY = void 0;

require('dotenv').config();

const KEY = process.env.KEY;
exports.KEY = KEY;
const SECRET = process.env.SECRET;
exports.SECRET = SECRET;