"use strict";

var _phrase = _interopRequireDefault(require("../../actions/phrase"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.getPhrase = ctx => {
  const response = (0, _phrase.default)();
  ctx.status = 200;
  ctx.body = {
    phrase: response
  };
};