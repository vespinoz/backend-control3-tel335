"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

const data = require("../data/phrases_data.js");

function getRandomPhrase() {
  var phrase = data[Math.floor(Math.random() * data.length)].text;
  return phrase;
}

var _default = getRandomPhrase;
exports.default = _default;