const data = require("../data/phrases_data.js");

function getRandomPhrase(){
    var phrase = data[Math.floor(Math.random()*data.length)].text;
    return phrase
}

export default getRandomPhrase