import koa from 'koa'
import bodyParser from 'koa-body'
import router from './routes/index'

const cors = require('@koa/cors'); //Linea Cors
const app = new koa()
const port = 3001


app.use(cors()) //Linea Cors

app.use(bodyParser({ multipart: true, urlencoded: true }))

app.use(router.routes())

app.listen(port, () => {
    console.log(`Server is running on port ${port}`)
})
