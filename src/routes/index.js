import Router from 'koa-router'
import Phrase from './phrase/index.js'

const router = new Router()

router.get('/phrase', Phrase.getPhrase)
export default router