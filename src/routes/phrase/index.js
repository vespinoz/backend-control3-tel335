import getRandomPhrase from '../../actions/phrase' 

exports.getPhrase = (ctx) => {
    const response = getRandomPhrase()
    ctx.status = 200
    ctx.body = {
        phrase: response
    }
}